<?php get_header(); ?>


<div id="content">

	<div id="inner-content"
	     class=" ">

		<main id="main"
		      class=" "
		      role="main"
		      itemscope
		      itemprop="mainContentOfPage"
		      itemtype="http://schema.org/Blog">

			<?php get_template_part( 'template-parts/section', 'block1' ); ?>
			<?php get_template_part( 'template-parts/section', 'numbers' ); ?>
			<?php get_template_part( 'template-parts/section', 'block2' ); ?>
			<?php get_template_part( 'template-parts/section', 'carousel' ); ?>
			<?php get_template_part( 'template-parts/section', 'students' ); ?>
			<?php get_template_part( 'template-parts/section', 'events' ); ?>
		</main>


	</div>

</div>

<?php get_footer(); ?>