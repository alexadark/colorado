<?php
/* Bones Custom Post Type Example
This page walks you through creating 
a custom post type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

Developed by: Eddie Machado
URL: http://themble.com/bones/
*/

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'bones_flush_rewrite_rules' );

// Flush your rewrite rules
function bones_flush_rewrite_rules() {
	flush_rewrite_rules();
}

add_action( 'init', 'register_students_post_type' );
function register_students_post_type() {

	$labels = array(
		'name'          => _x( 'Students', 'post type general name', 'webstantly' ),
		'singular_name' => _x( 'student', 'post type singular name', 'webstantly' ),
		'menu_name'     => _x( 'Students', 'admin menu name', 'webstantly' ),
		'add_new'       => _x( 'Add New student', 'faq', 'webstantly' ),
		'add_new_item'  => _x( 'Add New student', 'webstantly' ),
		'search_items'  => _x( 'Search student', 'webstantly' ),
		'not_found'     => _x( 'No student Found', 'webstantly' ),

	);
	$args   = array(
		'label'        => __( 'Students', 'webstantly' ),
		'labels'       => $labels,
		'supports'     => get_cpt_supports(),
		'public'       => true,
		'taxonomies'   => array(),
		'hierarchical' => true,
		'has_archive'  => true,
	);

	register_post_type( 'students', $args );
}

function get_cpt_supports() {
	$all_supports = get_all_post_type_supports( 'post' );


	$all_supports = array_keys( $all_supports );

	$supports_to_exclude = array(
		'comments',
		'trackbacks',
		'post_formats',
		'custom-fields',
	);


	$supports   = array_filter( $all_supports, function ( $support ) use ( $supports_to_exclude ) {
		return ! in_array( $support, $supports_to_exclude );
	} );
	$supports[] = 'page-attributes';

	return ( $supports );


}
//	/* this adds your post categories to your custom post type */
//	register_taxonomy_for_object_type( 'category', 'custom_type' );
//	/* this adds your post tags to your custom post type */
//	register_taxonomy_for_object_type( 'post_tag', 'custom_type' );
//
//}
//
//	// adding the function to the Wordpress init
//	add_action( 'init', 'custom_post_example');
//
//	/*
//	for more information on taxonomies, go here:
//	http://codex.wordpress.org/Function_Reference/register_taxonomy
//	*/
//
//	// now let's add custom categories (these act like categories)
//	register_taxonomy( 'custom_cat',
//		array('custom_type'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
//		array('hierarchical' => true,     /* if this is true, it acts like categories */
//			'labels' => array(
//				'name' => __( 'Custom Categories', 'bonestheme' ), /* name of the custom taxonomy */
//				'singular_name' => __( 'Custom Category', 'bonestheme' ), /* single taxonomy name */
//				'search_items' =>  __( 'Search Custom Categories', 'bonestheme' ), /* search title for taxomony */
//				'all_items' => __( 'All Custom Categories', 'bonestheme' ), /* all title for taxonomies */
//				'parent_item' => __( 'Parent Custom Category', 'bonestheme' ), /* parent title for taxonomy */
//				'parent_item_colon' => __( 'Parent Custom Category:', 'bonestheme' ), /* parent taxonomy title */
//				'edit_item' => __( 'Edit Custom Category', 'bonestheme' ), /* edit custom taxonomy title */
//				'update_item' => __( 'Update Custom Category', 'bonestheme' ), /* update title for taxonomy */
//				'add_new_item' => __( 'Add New Custom Category', 'bonestheme' ), /* add new title for taxonomy */
//				'new_item_name' => __( 'New Custom Category Name', 'bonestheme' ) /* name title for taxonomy */
//			),
//			'show_admin_column' => true,
//			'show_ui' => true,
//			'query_var' => true,
//			'rewrite' => array( 'slug' => 'custom-slug' ),
//		)
//	);
//
//	// now let's add custom tags (these act like categories)
//	register_taxonomy( 'custom_tag',
//		array('custom_type'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
//		array('hierarchical' => false,    /* if this is false, it acts like tags */
//			'labels' => array(
//				'name' => __( 'Custom Tags', 'bonestheme' ), /* name of the custom taxonomy */
//				'singular_name' => __( 'Custom Tag', 'bonestheme' ), /* single taxonomy name */
//				'search_items' =>  __( 'Search Custom Tags', 'bonestheme' ), /* search title for taxomony */
//				'all_items' => __( 'All Custom Tags', 'bonestheme' ), /* all title for taxonomies */
//				'parent_item' => __( 'Parent Custom Tag', 'bonestheme' ), /* parent title for taxonomy */
//				'parent_item_colon' => __( 'Parent Custom Tag:', 'bonestheme' ), /* parent taxonomy title */
//				'edit_item' => __( 'Edit Custom Tag', 'bonestheme' ), /* edit custom taxonomy title */
//				'update_item' => __( 'Update Custom Tag', 'bonestheme' ), /* update title for taxonomy */
//				'add_new_item' => __( 'Add New Custom Tag', 'bonestheme' ), /* add new title for taxonomy */
//				'new_item_name' => __( 'New Custom Tag Name', 'bonestheme' ) /* name title for taxonomy */
//			),
//			'show_admin_column' => true,
//			'show_ui' => true,
//			'query_var' => true,
//		)
//	);
	
	/*
		looking for custom meta boxes?
		check out this fantastic tool:
		https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
	*/
	

?>
