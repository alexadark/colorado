<?php get_header();
get_template_part( 'template-parts/content', 'page' );

if ( ! is_archive() && ! is_single() ):
	get_template_part( 'template-parts/section', 'block-page' );
	get_template_part( 'template-parts/section', 'page-section' );
endif; ?>


	</main>


	</div>

	</div>


<?php
get_footer();