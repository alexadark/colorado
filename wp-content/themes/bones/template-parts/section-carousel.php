<section class="logo-carousel">
	<div class="flexslider ">
		<h2 class="section-title">Prostart High Schools</h2>
		<?php if(have_rows('wst_logo')):?>
			<ul class="slides">
				<?php while(have_rows('wst_logo')): the_row();
					$image = get_sub_field('wst_logo_image');
					?>
					<li>
						<a href="<?php the_sub_field('wst_logo_link');?>" target="_blank">
							<img src="<?php echo $image['url'];?>"
							     alt="<?php echo $image['alt'];?>">
						</a>
					</li>
				<?php endwhile;?>
			</ul>
		<?php endif;?>
	</div>
</section>