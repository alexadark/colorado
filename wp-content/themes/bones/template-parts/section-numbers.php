<section class="numbers cf">
	<div class="wrap">
		<div class="number-item">
			<img class="schools" src="<?php echo IMG ?>schools.png"
			     alt="schools">

			<div class="number">6</div>
			<div class="title">Schools</div>
		</div>
		<div class="number-item">
			<img src="<?php echo IMG ?>students.png"
			     alt="students">
			<div class="number">456</div>
			<div class="title">Students</div>
		</div>
		<div class="number-item">
			<img src="<?php echo IMG ?>classes.png"
			     alt="classes">
			<div class="number">27</div>
			<div class="title">Classes</div>
		</div>
		<div class="number-item">
			<img src="<?php echo IMG ?>years.png"
			     alt="years">
			<div class="number">16</div>
			<div class="title">Years</div>
		</div>
		<div class="number-item">
			<img src="<?php echo IMG ?>scholar.png"
			     alt="scholarships">
			<div class="number">377</div>
			<div class="title">Scholarships</div>
		</div>
	</div>
</section>