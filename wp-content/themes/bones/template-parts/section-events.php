<section class="events">
	<div class="events-container cf">
	<?php
	$args = array(
		'post_type'      => array( 'tribe_events' ),
		'posts_per_page' => 2,
	);

	$events = new WP_Query( $args );

	if ( $events->have_posts() ) : ?>
		<div class="events-col ">
			<h2 class="section-title">Events</h2>
			<?php while ( $events->have_posts() ) :
			$events->the_post();
			?>

			<div class="event-item">
				<div class="event-date">
					<?php echo tribe_get_start_date(); ?>
				</div>
				<div class="event-title">
					<a href="<?php the_permalink(); ?>">
						<?php the_title(); ?>
					</a>
				</div>
				<div class="event-venue">
					<?php echo tribe_get_venue(); ?>
				</div>
			</div>
		<?php endwhile;
		echo '<div class=\'calendar\'><a href="/events"><h3 class="green-block-title">Calendar</h3></a></div>';
		echo '</div>';
	endif;


	// Reset original post data
	wp_reset_postdata();

	?>
		<div class="image-col">
			<div class="event-image"
			     style="background:url(<?php the_field( 'first_event_image' ); ?>);
				     -webkit-background-size:cover ;background-size: cover;"></div>
			<div class="event-image"
			     style="background:url(<?php the_field( 'second_event_image' ); ?>);
				     -webkit-background-size:cover ;background-size: cover;"></div>
		</div>
		<div class="logo-col">
			<div class="event-logo">
				<?php $image = get_field('first_event_logo');?>
				<img src="<?php echo $image['url'];?>"
				     alt="<?php echo $image['alt'];?>">
			</div>
			<div class="event-logo">
				<?php $image = get_field('second_event_logo');?>
				<img src="<?php echo $image['url'];?>"
				     alt="<?php echo $image['alt'];?>">
			</div>
		</div>
	</div>
</section>