<section class="home-block cf ">
	<?php if ( get_field( 'home_block_1_modal_video' ) ): ?>
	<a class="<?php the_field( 'home_block_1_modal_video' ); ?>"

	   href="#">
		<?php endif; ?>

		<div class="left-block "
		     style="background:url(<?php the_field( 'home_block_1_big_image' ); ?>);
			     -webkit-background-size:cover ;background-size: cover;">

		</div>
		<?php if ( get_field( 'home_block_1_modal_video' ) ): ?>
	</a>
<?php endif; ?>
	<div class="right-block ">
		<div class="text-block ">
			<h3 class="text-block-title"><?php the_field( 'home_block_1_title' ); ?></h3>
			<div class="text-block-content">
				<?php the_field( 'home_block_1_content' ); ?>
			</div>

		</div>
		<div class="bottom-right-block ">
			<div class="image-small "
			     style="background:url(<?php the_field( 'home_block_1_small_image' ); ?>);
				     -webkit-background-size:cover ;background-size: cover;"></div>
			<?php if ( get_field( 'home_block_1_green_block_link' ) ): ?>
			<a href="<?php the_field( 'home_block_1_green_block_link' ); ?>">
				<?php endif; ?>
				<div class="green-block half ">
					<h3 class="green-block-title"><?php the_field( 'home_block_1_green_block_title' ); ?></h3>
				</div>
				<?php if ( get_field( 'home_block_1_green_block_link' ) ): ?>
			</a>
		<?php endif; ?>
		</div>
	</div>
</section>