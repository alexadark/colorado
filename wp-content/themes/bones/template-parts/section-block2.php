<section class="home-block second cf ">

	<div class="left-block  ">
		<div class="text-block ">
			<h3 class="text-block-title"><?php the_field( 'second_block_title' ); ?></h3>
			<div class="text-block-content">
				<?php the_field( 'second_block_content' ); ?>
			</div>

		</div>
		<div class="bottom-right-block ">

			<?php if ( get_field ('second_block_green_link' ) ): ?>
			<a href="<?php the_field( 'second_block_green_link' ); ?>">
				<?php endif; ?>
				<div class="green-block">
					<h3 class="green-block-title"><?php the_field( 'second_block_green_title' ); ?></h3>
				</div>
				<?php if ( get_field( 'second_block_green_link' ) ): ?>
			</a>
		<?php endif; ?>
		</div>
	</div>

	<?php if ( get_field( 'second_block_modal_video' ) ): ?>
	<a class="<?php the_field( 'second_block_modal_video' ); ?>"

	   href="#">
		<?php endif; ?>

		<div class="right-block "
		     style="background:url(<?php the_field( 'second_block_big_image' ); ?>);
			     -webkit-background-size:cover ;background-size: cover;">

		</div>
		<?php if ( get_field( 'second_block_modal_video' ) ): ?>
	</a>
<?php endif;?>
</section>