<section class="students">
	<?php
	$args = array(
		'post_type'=> array('students'),
	);
	if(is_front_page()){
		$args = array(
			'post_type'=> array('students'),
			'meta_query' => array(
				array(
					'key' => 'student_home',
					'value' => '1',
				),
			),
		);
	}
	$students = new WP_Query($args);

	if ( $students->have_posts() ) :
		echo '<div class="students-container cf">';
		while ( $students->have_posts() ) :
			$students->the_post(); ?>
			<div class="student">
				<a href="<?php the_permalink();?>">
					<div class="student-picture"
					     style="background:url(<?php the_post_thumbnail_url( 'full' ); ?>);
						     -webkit-background-size:cover ;background-size: cover; min-height: 344px;">

					</div>
				</a>
				<h3 class="student-name text-block-title"><a href="<?php the_permalink();?>"><?php
						the_title();
						?></a></h3>
				<div class="student-excerpt"> <?php the_excerpt(); ?></div>
			</div>
		<?php endwhile;
		echo '</div>';
	endif;


	// Reset original post data
	wp_reset_postdata();
	?>
</section>