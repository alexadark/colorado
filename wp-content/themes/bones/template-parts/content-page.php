<div id="content">

	<div id="inner-content"
	     class=" cf">

		<main id="main"
		      class=" cf"
		      role="main"
		      itemscope
		      itemprop="mainContentOfPage"
		      itemtype="http://schema.org/Blog">

			<section class="intro">
				<h2 class="section-title"><?php the_title(); ?></h2>

				<div class="intro-container wrap cf">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
						the_content();
					endwhile;
					else :
					endif; ?>

				</div>
			</section>