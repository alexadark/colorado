<section class="home-block page-block cf ">

		<div class="left-block " >
			<?php the_field('page_block_content');?>

		</div>

	<div class="right-block ">
		<div class="top-image"
		     style="background:url(<?php the_field( 'page_block_image_top' ); ?>);
			     -webkit-background-size:cover ;background-size: cover;">

		</div>
		<div class="bottom-right-block ">
			<div class="image-small "
			     style="background:url(<?php the_field( 'page_block_image_left' ); ?>);
				     -webkit-background-size:cover ;background-size: cover;"></div>

				<div class="image-small half "
				     style="background:url(<?php the_field( 'page_block_image_right' ); ?>);
					     -webkit-background-size:cover ;background-size: cover;">

				</div>

		</div>
	</div>
</section>