<section class="three-blocks cf">
	<div class="block green">
		<?php if ( get_field( 'first_block_link' ) ): ?>
		<a href="<?php the_field( 'first_block_link' ); ?>">
			<?php endif; ?>
			<h3 class="green-block-title"><?php the_field( 'first_block_title' ); ?></h3>
			<?php if ( get_field( 'first_block_link' ) ): ?>
		</a>
	<?php endif; ?>
	</div>
	<div class="block">
		<?php if ( get_field( 'second_block_link' ) ): ?>
		<a href="<?php the_field( 'second_block_link' ); ?>">
			<?php endif; ?>
			<img src="<?php echo IMG ?>prostart.png"
			     alt="prostart">
			<?php if ( get_field( 'second_block_link' ) ): ?>
		</a>
	<?php endif; ?>
	</div>
	<div class="block green">
		<?php if ( get_field( 'third_block_link' ) ): ?>
		<a href="<?php the_field( 'third_block_link' ); ?>">
			<?php endif; ?>
			<h3 class="green-block-title"><?php the_field( 'third_block_title' ); ?></h3>
			<?php if ( get_field( 'third_block_link' ) ): ?>
		</a>
	<?php endif; ?>
	</div>

</section>