<section class="page-section cf">
	<div class="block green">
			<h3 class="green-block-title"><?php the_field('page_section_1_title');?></h3>
		<div class="page-section-content">
			<?php the_field('page_section_1_content');?>
		</div>
		<div class="trigger">+</div>

	</div>
	<div class="block">
		<h3 class="green-block-title"><?php the_field('page_section_2_title');?></h3>
		<div class="page-section-content">
			<?php the_field('page_section_2_content');?>
		</div>
		<div class="trigger">+</div>
	</div>
	<div class="block green">
		<h3 class="green-block-title"><?php the_field('page_section_3_title');?></h3>
		<div class="page-section-content">
			<?php the_field('page_section_3_content');?>
		</div>
		<div class="trigger">+</div>
	</div>

</section>