<section class="curriculum">
	<h2 class="section-title">Curriculum</h2>
	<div class="curriculum-container wrap cf">
		<div class="left">
			<div class="cf">
				<div class="icon">
					<img
						<?php $icon = get_field( 'curriculum_icon_left' ); ?>
						src="<?php echo $icon['url']; ?>"
						alt="<?php echo $icon['alt']; ?>"/>
				</div>
				<div class="content"><?php the_field( 'curriculum_content_left' ); ?></div>
			</div>

			<h3 class="title"><a href="<?php the_field( 'curriculum_link_left' ); ?>"><?php the_field
					( 'curriculum_title_left' ); ?></a></h3>

		</div>
		<div class="right">
			<div class="cf">
				<div class="icon">
					<img
						<?php $icon = get_field( 'curriculum_icon_right' ); ?>
						src="<?php echo $icon['url']; ?>"
						alt="<?php echo $icon['alt']; ?>"/>

				</div>
				<div class="content"><?php the_field( 'curriculum_content_right' ); ?></div>
			</div>
			<h3 class="title"><a href="<?php the_field( 'curriculum_link_left' ); ?>"><?php the_field
					( 'curriculum_title_right' ); ?></a>
				<h3/>

		</div>
	</div>
</section>