<?php
//Template name: Landing page
get_header( 'landing' ); ?>
	<section class="landing-contact"
	         style="background: url(<?php the_field( 'landing_background_image' ); ?>);
		         -webkit-background-size:cover ;
		         background-size: cover;">
		<div class="wrap cf">
			<div class="left-block text">
				<h2><?php the_field( 'form_text' ); ?></h2>
			</div>
			<div class="right-block form">

			</div>
		</div>
	</section>
<?php get_template_part( 'template-parts/section', 'numbers' ); ?>
	<section class="gallery cf">
<?php if ( have_rows( 'landing_image_gallery' ) ): ?>
	<section class="gallery cf">
	<?php while ( have_rows( 'landing_image_gallery' ) ): the_row(); ?>

	<div class="gallery-item"
	     style="background: url(<?php the_sub_field( 'image_item' ); ?>);
		     -webkit-background-size:cover ;
		     background-size: cover;">
	</div>

<?php endwhile;
	echo '</section>';
endif; ?>


<?php
get_footer();