<?php get_header(); ?>

			<div id="content">
            
           	<!-- BEGIN SLIDER INCLUDE -->
                    <div id="slider-container">
                        <div id="hp-slider" class="flexslider">
                            <ul class="slides">
                            <?php
                                $sliderimages = new WP_Query();
                                $sliderimages->query('posts_per_page=10&orderby=rand&&post_type=hp_slider_images');
                                if ($sliderimages->have_posts()):
                                while ($sliderimages->have_posts()) : $sliderimages->the_post();
                            ?>
                                <li>
                                    <?php the_post_thumbnail('full'); ?>
                                    <div id="slider-title">
                                    </div>
                                </li>
                                <?php wp_reset_postdata(); ?>
                            <?php endwhile; endif; ?>
                            </ul>
                        </div>
                    </div>
            <!-- END SLIDER INCLUDE -->
            
            
            
            <!-- FEATURED BOXES -->
        		<section class="entry">
          			<div id="feature-container">
                 		<?php
							$args = array('post_type' => 'featured_boxes', 'posts_per_page' => 20, 'tag__not_in' => array( 9 ) );
							$loop = new WP_Query($args);
							while ($loop->have_posts()) : $loop->the_post(); ?>
								<div class="other-fprojects desktoponly" id="other-fprojects">
								<h3><a href="<?php the_field('links'); ?>"><?php the_title(); ?></a></h3>
							  	<div class="fimage-contain">
                                	<a href="<?php the_field('links'); ?>" target="<?php the_field('new_window'); ?>">
										<?php the_post_thumbnail('full'); ?>
                                	</a>
                                </div>
                                <?php the_excerpt(); ?> 
								<div id="learnmorebutton" style="width: 100%; text-align:leftr; font-size:12px;">
                                	<a style="text-align:left;" href="<?php the_field('links'); ?>">Learn More</a>
                                </div> 
							</div>   
						<?php endwhile; ?>
                        </div>
               	  </section><!-- /custom fields for home -->
                  <!-- END FEATURED BOXES -->

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="eightcol first clearfix" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

								<header class="article-header">
									<h1 class="hp-title"><?php the_title(); ?></h1>
								</header> <!-- end article header -->
								<section class="entry-content clearfix">
									<?php the_content(); ?>
								</section> <!-- end article section -->

								<footer class="article-footer">
									<p class="tags"><?php the_tags('<span class="tags-title">' . __('Tags:', 'bonestheme') . '</span> ', ', ', ''); ?></p>

								</footer> <!-- end article footer -->

								<?php // comments_template(); // uncomment if you want to use them ?>

							</article> <!-- end article -->

							<?php endwhile; ?>

									<?php if (function_exists('bones_page_navi')) { ?>
											<?php bones_page_navi(); ?>
									<?php } else { ?>
											<nav class="wp-prev-next">
													<ul class="clearfix">
														<li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "bonestheme")) ?></li>
														<li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "bonestheme")) ?></li>
													</ul>
											</nav>
									<?php } ?>

							<?php else : ?>

									<article id="post-not-found" class="hentry clearfix">
											<header class="article-header">
												<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e("This is the error message in the index.php template.", "bonestheme"); ?></p>
										</footer>
									</article>

							<?php endif; ?>
                    

						</div> <!-- end #main -->

						<?php get_sidebar(); ?>

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
