			<footer class="footer" role="contentinfo">
            
                    



				<div id="inner-footer" class="wrap clearfix">

					<nav role="navigation"> 
						<?php bones_footer_links(); ?>
					</nav>

				</div> <!-- end #inner-footer -->

			</footer> <!-- end footer -->

		</div> <!-- end #container -->


		<!-- all js scripts are loaded in library/bones.php -->
		<?php wp_footer(); ?> 

<script type="text/javascript">
					$(window).load(function() {
						$('.flexslider').flexslider({
							slideshow: "true",
							animation: "fade"
						});

					});
</script>
	</body>

</html> <!-- end page. what a ride! -->
