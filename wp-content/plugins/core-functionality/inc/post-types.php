<?php
// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'wst_flush_rewrite_rules' );

// Flush your rewrite rules
function wst_flush_rewrite_rules() {
	flush_rewrite_rules();
}
add_action( 'init', 'register_students_post_type' );
function register_students_post_type() {

	$labels = array(
		'name'          => _x( 'Students', 'post type general name', 'bones' ),
		'singular_name' => _x( 'student', 'post type singular name', 'bones' ),
		'menu_name'     => _x( 'Students', 'admin menu name', 'bones' ),
		'add_new'       => _x( 'Add New student', 'faq', 'bones' ),
		'add_new_item'  => _x( 'Add New student', 'bones' ),
		'search_items'  => _x( 'Search student', 'bones' ),
		'not_found'     => _x( 'No student Found', 'bones' ),

	);
	$args   = array(
		'label'        => __( 'Students', 'bones' ),
		'labels'       => $labels,
		'supports'     => get_cpt_supports(),
		'public'       => true,
		'taxonomies'   => array(),
		'hierarchical' => true,
		'has_archive'  => true,
		'rewrite' =>array('slug' => 'students-archive'),
	);

	register_post_type( 'students', $args );
}

function get_cpt_supports() {
	$all_supports = get_all_post_type_supports( 'post' );


	$all_supports = array_keys( $all_supports );

	$supports_to_exclude = array(
		'comments',
		'trackbacks',
		'post_formats',
		'custom-fields',
	);


	$supports   = array_filter( $all_supports, function ( $support ) use ( $supports_to_exclude ) {
		return ! in_array( $support, $supports_to_exclude );
	} );
	$supports[] = 'page-attributes';

	return ( $supports );


}
